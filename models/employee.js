const Joi = require("joi");
const mongoose = require("mongoose");
const { designationSchema } = require("./designation");

const Employee = mongoose.model(
  "Employees",
  new mongoose.Schema({
    name: {
      type: String,
      required: true,
      trim: true,
      minlength: 3,
      maxlength: 255
    },
    designation: {
      type: designationSchema,
      required: true
    },
    salary: {
      type: Number,
      required: true,
      min: 0
    },
    joinedDate: {
      type: String,
      required: true
    },
    active: {
      type: Boolean
    }
  })
);

function validateEmployee(employee) {
  const schema = {
    name: Joi.string()
      .min(3)
      .max(50)
      .required(),
    designationId: Joi.objectId().required(),
    salary: Joi.number()
      .min(0)
      .required(),
    joinedDate: Joi.string().required()
  };

  return Joi.validate(employee, schema);
}

exports.Employee = Employee;
exports.validate = validateEmployee;
