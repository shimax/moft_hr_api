const Joi = require("joi");
const mongoose = require("mongoose");

const designationSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 50
  }
});

const Designation = mongoose.model("Designations", designationSchema);

function validateDesignation(designation) {
  const schema = {
    name: Joi.string()
      .min(3)
      .max(50)
      .required()
  };

  return Joi.validate(designation, schema);
}

exports.designationSchema = designationSchema;
exports.Designation = Designation;
exports.validate = validateDesignation;
