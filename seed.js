const { Designation } = require("./models/designation");
const { Employee } = require("./models/employee");
const mongoose = require("mongoose");
const config = require("config");

const data = [
  {
    name: "Human resouce manager",
    employees: [
      {
        name: "Employee 1",
        salary: 30000,
        joinedDate: "01/03/2018",
        active: true
      }
    ]
  },
  {
    name: "Human resource assistant",
    employees: [
      {
        name: "Employee 2",
        salary: 25000,
        joinedDate: "01/03/2018",
        active: true
      },
      {
        name: "Employee 3",
        salary: 50000,
        joinedDate: "01/03/2018",
        active: false
      }
    ]
  },
  {
    name: "Human resource office",
    employees: [
      {
        name: "Employee 4",
        salary: 25000,
        joinedDate: "01/03/2018",
        active: true
      },
      {
        name: "Employee 5",
        salary: 25000,
        joinedDate: "01/03/2018",
        active: false
      },
      {
        name: "Employee 6",
        salary: 25000,
        joinedDate: "01/03/2018",
        active: true
      },
      {
        name: "Employee 7",
        salary: 25000,
        joinedDate: "01/03/2018",
        active: false
      },
      {
        name: "Employee 8",
        salary: 50000,
        joinedDate: "01/03/2018",
        active: true
      },
      {
        name: "Employee 9",
        salary: 40000,
        joinedDate: "01/03/2018",
        active: false
      }
    ]
  }
];

async function seed() {
  await mongoose.connect(config.get("db"));

  await Employee.deleteMany({});
  await Designation.deleteMany({});

  for (let designation of data) {
    const { _id: designationId } = await new Designation({
      name: designation.name
    }).save();
    const employees = designation.employees.map(employee => ({
      ...employee,
      designation: { _id: designationId, name: designation.name }
    }));
    await Employee.insertMany(employees);
  }

  mongoose.disconnect();

  console.info("Done!");
}

seed();
