const express = require("express");
const designations = require("../routes/designations");
const employees = require("../routes/employees");
const users = require("../routes/users");
const auth = require("../routes/auth");
const error = require("../middleware/error");

module.exports = function(app) {
  app.use(express.json());
  app.use("/api/designations", designations);
  app.use("/api/employees", employees);
  app.use("/api/users", users);
  app.use("/api/auth", auth);
  app.use(error);
};
