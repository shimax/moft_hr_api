const validateObjectId = require("../middleware/validateObjectId");
const { Designation, validate } = require("../models/designation");
const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();

router.get("/", async (req, res) => {
  const designations = await Designation.find()
    .select("-__v")
    .sort("name");
  res.send(designations);
});

router.post("/", [auth], async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let designation = new Designation({ name: req.body.name });
  await designation.save();

  res.send(designation);
});

router.put("/:id", [auth, validateObjectId], async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const designation = await Designation.findByIdAndUpdate(
    req.params.id,
    { name: req.body.name },
    {
      new: true
    }
  );

  if (!designation)
    return res
      .status(404)
      .send("The designation with the given ID was not found.");

  res.send(designation);
});

router.get("/:id", validateObjectId, async (req, res) => {
  const designation = await Designation.findById(req.params.id).select("-__v");

  if (!designation)
    return res
      .status(404)
      .send("The designation with the given ID was not found.");

  res.send(designation);
});

router.delete("/:id", [auth, admin, validateObjectId], async (req, res) => {
  const designation = await Designation.findByIdAndRemove(req.params.id);

  if (!designation)
    return res
      .status(404)
      .send("The designation with the given ID was not found.");

  res.send(genre);
});

module.exports = router;
