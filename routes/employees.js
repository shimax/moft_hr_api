const { Employee, validate } = require("../models/employee");
const { Designation } = require("../models/designation");
const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
const validateObjectId = require("../middleware/validateObjectId");
const moment = require("moment");
const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();

router.get("/", async (req, res) => {
  const employees = await Employee.find()
    .select("-__v")
    .sort("name");
  res.send(employees);
});

router.post("/", [auth], async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const designation = await Designation.findById(req.body.designationId);
  if (!designation) return res.status(400).send("Invalid designation.");

  const employee = new Employee({
    name: req.body.name,
    designation: {
      _id: designation._id,
      name: designation.name
    },
    salary: req.body.salary,
    joinedDate: moment().toJSON(req.body.joinedDate),
    active: true
  });
  await employee.save();

  res.send(employee);
});

router.put("/:id", [auth], async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const designation = await Designation.findById(req.body.designationId);
  if (!designation) return res.status(400).send("Invalid deisignation.");

  const employee = await Employee.findByIdAndUpdate(
    req.params.id,
    {
      name: req.body.name,
      designation: {
        _id: designation._id,
        name: designation.name
      },
      salary: req.body.salary,
      joinedDate: moment().toJSON(req.body.joinedDate)
    },
    { new: true }
  );

  if (!employee)
    return res
      .status(404)
      .send("The employee with the given ID was not found.");

  res.send(employee);
});

router.get("/:id", async (req, res) => {
  const employees = await Employee.findById(req.params.id).select("-__v");

  if (!employees)
    return res
      .status(404)
      .send("The Employee with the given ID was not found.");

  res.send(employees);
});

router.delete("/:id", [auth, admin], async (req, res) => {
  const employee = await Employee.findByIdAndRemove(req.params.id);

  if (!employee)
    return res
      .status(404)
      .send("The employeed with the given ID was not found.");

  res.send(employee);
});

module.exports = router;
